// Copyright (C) 2019 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0

#include <QCoreApplication>
#include <QFile>
#include <QTextStream>

#include <QtQml/private/qqmljslexer_p.h>
#include <QtQml/private/qqmljsparser_p.h>
#include <QtQml/private/qqmljsengine_p.h>
#include <QtQml/private/qqmljsastvisitor_p.h>
#include <QtQml/private/qqmljsast_p.h>
#include <QtQmlDom/private/qqmldomitem_p.h>
#include <QtQmlDom/private/qqmldomexternalitems_p.h>
#include <QtQmlDom/private/qqmldomtop_p.h>
#include <QtQmlDom/private/qqmldomoutwriter_p.h>

#include <QCommandLineParser>

using namespace QQmlJS::Dom;

bool parseFile(const QString &filename)
{
    DomItem env =
            DomEnvironment::create(QStringList(),
                                   QQmlJS::Dom::DomEnvironment::Option::SingleThreaded
                                           | QQmlJS::Dom::DomEnvironment::Option::NoDependencies);

    DomItem tFile; // place where to store the loaded file
    env.loadFile(
            filename, QString(),
            [&tFile](Path, const DomItem &, const DomItem &newIt) {
                tFile = newIt; // callback called when everything is loaded that receives the loaded
                               // external file pair (path, oldValue, newValue)
            },
            LoadOption::DefaultLoad);
    env.loadPendingDependencies();
    DomItem qmlFile = tFile.fileObject();
    std::shared_ptr<QmlFile> qmlFilePtr = qmlFile.ownerAs<QmlFile>();
    if (!qmlFilePtr || !qmlFilePtr->isValid()) {
        qmlFile.iterateErrors(
                [](DomItem, ErrorMessage msg) {
                    errorToQDebug(msg);
                    return true;
                },
                true);
        qWarning().noquote() << "Failed to parse" << filename;
        return false;
    }

    // DAVE code here!

    qmlFile.visitTree(Path(), [](Path, DomItem &item, bool) -> bool {
        if (item.internalKind() == DomType::Import) {
            MutableDomItem mutableItem(item);
            Import *import = mutableItem.mutableAs<Import>();

            if (import->uri.moduleUri() == "QtGraphicalEffects") {
                import->uri = QmlUri::fromUriString("Qt5Compat.GraphicalEffects");
                import->version = Version(6, 0);
                mutableItem.commitToBase();
            }
        }

        if (item.internalKind() == DomType::QmlObject) {
            // Dave - TODO use the resolved name to use the resolved namespace


            if (item.name().contains("DropShadow")) {
                // qDebug() << item;
                // qDebug() << item.as<QmlObject>()->prototypePaths();
                MutableDomItem mutableItem(item);
                QmlObject *dropShadow = mutableItem.mutableAs<QmlObject>();
                auto bindings = dropShadow->bindings();
                bindings.remove("samples");
                dropShadow->setBindings(bindings);
                mutableItem.commitToBase();
            }
        }

        return true;
    });



    // reload modifications
    qmlFile = qmlFile.refreshed();


    // end DAVE code




    // Turn AST back into source code

    LineWriterOptions lwOptions;
    lwOptions.updateOptions = LineWriterOptions::Update::None;
    lwOptions.attributesSequence = LineWriterOptions::AttributesSequence::Preserve;

    WriteOutChecks checks = WriteOutCheck::UpdatedDomStable;

    MutableDomItem res;
    if (false) { // inplace
        qWarning().noquote() << "Writing to file" << filename;
        FileWriter fw;
        const unsigned numberOfBackupFiles = 0;
        res = qmlFile.writeOut(filename, numberOfBackupFiles, lwOptions, &fw, checks);
    } else {
        QFile out;
        out.open(stdout, QIODevice::WriteOnly);
        LineWriter lw([&out](QStringView s) { out.write(s.toUtf8()); }, filename, lwOptions);
        OutWriter ow(lw);
        res = qmlFile.writeOutForFile(ow, checks);
        ow.flush();
    }
    return bool(res);
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("qmlformat");
    QCoreApplication::setApplicationVersion(QT_VERSION_STR);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    parser.process(app);

    bool success = true;
    for (const QString &file : parser.positionalArguments()) {
        qWarning() << "parsing file" << file;
        parseFile(file);
    }

    return success ? 0 : 1;
}
